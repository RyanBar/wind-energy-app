package com.mecheng.ryanbarr.windenergy;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Button;
import android.widget.SeekBar.OnSeekBarChangeListener;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.GridLabelRenderer;
import android.view.View;
import android.widget.Switch;

import java.util.Random;

///**
// * Created by ryanbarr on 9/3/15.
// */
public class ActivityLiftDrag extends AppCompatActivity {
    public SeekBar seekBar;
    public TextView textAngle;
    public TextView textLift;
    public TextView textDrag;
    public ImageView airfoilImage;
    public GraphView airfoil_graph;
    public LineGraphSeries<DataPoint> topSurface;
    public LineGraphSeries<DataPoint> bottomSurface;
    public LineGraphSeries<DataPoint> liftLine;
    public LineGraphSeries<DataPoint> dragLine;
    public TextView textStall;
    public TextView textLD;
    public TextView textLDmessage;
    public double LD = 0;
    public Switch switchAirfoil;
    public int airfoil_type = 0;
    public double set_alpha = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liftdrag);

        seekBar = (SeekBar) findViewById(R.id.seekAngle);
        textAngle = (TextView) findViewById(R.id.textAngle);
        textLift = (TextView) findViewById(R.id.textLift);
        textDrag = (TextView) findViewById(R.id.textDrag);
        textStall = (TextView) findViewById(R.id.textStall);
        airfoilImage = (ImageView) findViewById(R.id.airfoil);
        airfoil_graph = (GraphView) findViewById(R.id.graph);
        textLD = (TextView) findViewById(R.id.textLD);
        textLDmessage = (TextView) findViewById(R.id.textLDmessage);
        switchAirfoil = (Switch) findViewById(R.id.switchAirfoil);

        textStall.setTextColor(0xffff8800);

        Viewport view_airfoil = airfoil_graph.getViewport();
        view_airfoil.setXAxisBoundsManual(true);
        view_airfoil.setYAxisBoundsManual(true);
        view_airfoil.setMaxX(1.0);
        view_airfoil.setMaxY(0.35);
        view_airfoil.setMinY(-0.35);
        GridLabelRenderer grid = airfoil_graph.getGridLabelRenderer();
        grid.setHorizontalLabelsVisible(false);
        grid.setVerticalLabelsVisible(false);
        grid.setGridStyle(GridLabelRenderer.GridStyle.NONE);
        topSurface = new LineGraphSeries<DataPoint>(generateAirfoil(1, 0));
        bottomSurface = new LineGraphSeries<DataPoint>(generateAirfoil(-1, 0));
        airfoil_graph.addSeries(topSurface);
        airfoil_graph.addSeries(bottomSurface);
        topSurface.setBackgroundColor(0xff0099cc);
        topSurface.setDrawBackground(true);
        bottomSurface.setBackgroundColor(0xffff8800);
        bottomSurface.setDrawBackground(true);


        Button button_main = (Button)findViewById(R.id.buttonMenu);
        button_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_a = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent_a);
            }
        });

        switchAirfoil.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                double cl;
                double cd;
                double alpha = set_alpha;
                if (isChecked) {
                    airfoil_type = 1;
                    topSurface.resetData(generateAirfoilDU(1, set_alpha));
                    bottomSurface.resetData(generateAirfoilDU(-1, set_alpha));
                    cl = 2E-09 * Math.pow(alpha, 6) + 5E-07 * Math.pow(alpha, 5) + 4E-06 * Math.pow(alpha, 4) - 0.0004 * Math.pow(alpha, 3) - 0.0024 * Math.pow(alpha, 2) + 0.1447 * alpha + 0.4665;
                    cd = -5E-09 * Math.pow(alpha, 6) - 2E-08 * Math.pow(alpha, 5) + 3E-06 * Math.pow(alpha, 4) + 1E-05 * Math.pow(alpha, 3) - 9E-05 * Math.pow(alpha, 2) - 0.0004 * alpha + 0.0075;
                    topSurface.setBackgroundColor(0xff669900);
                    topSurface.setColor(0xff669900);
                    bottomSurface.setColor(0xff669900);

                } else {
                    airfoil_type = 0;
                    topSurface.resetData(generateAirfoil(1, set_alpha));
                    bottomSurface.resetData(generateAirfoil(-1, set_alpha));
                    cl = -2E-06 * Math.pow(alpha, 4) - 0.0002 * Math.pow(alpha, 3) + 0.0006 * Math.pow(alpha, 2) + 0.1132 * alpha;
                    cd = 8E-07 * Math.pow(alpha, 4) + 2E-06 * Math.pow(alpha, 3) + 0.0003 * Math.pow(alpha, 2) - 0.001 * alpha + 0.01;
                    topSurface.setBackgroundColor(0xff0099cc);
                    topSurface.setColor(0xff0099cc);
                    bottomSurface.setColor(0xff0099cc);
                }

                textLift.setText(String.format("%.4f", cl));
                textDrag.setText(String.format("%.4f", cd));
            }
        });



        double alpha = 4.8;
        double cl = -2E-06*Math.pow(alpha,4) - 0.0002*Math.pow(alpha,3) + 0.0006*Math.pow(alpha,2) + 0.1132*alpha;
        double cd = 8E-07*Math.pow(alpha,4) + 2E-06*Math.pow(alpha,3) + 0.0003*Math.pow(alpha,2) - 0.001*alpha + 0.01;
        double angle = Math.toRadians(Math.abs(alpha));
        double x_lift_new = 0.25*Math.cos(angle) + 0.0*Math.sin(angle);
        double y_lift_new = -0.25*Math.sin(angle) + 0.0*Math.cos(angle);
        double x_lift_new_2 = 0.25*Math.cos(angle) + cd*Math.sin(angle)+0.02;
        double y_lift_new_2 = -0.25*Math.sin(angle) + cl*0.25*Math.cos(angle);
        DataPoint[] lift_arrow = new DataPoint[2];
        DataPoint lift_arrow_1 = new DataPoint(x_lift_new, y_lift_new);
        DataPoint lift_arrow_2 = new DataPoint(x_lift_new, y_lift_new_2);
        lift_arrow[0] = lift_arrow_1;
        lift_arrow[1] = lift_arrow_2;
        liftLine = new LineGraphSeries<DataPoint>(lift_arrow);
        liftLine.setThickness(10);
        liftLine.setColor(Color.LTGRAY);
        liftLine.setTitle("Lift");
        airfoil_graph.addSeries(liftLine);

        DataPoint[] drag_arrow = new DataPoint[2];
        DataPoint drag_arrow_1 = new DataPoint(x_lift_new, y_lift_new);
        DataPoint drag_arrow_2 = new DataPoint(x_lift_new_2, y_lift_new);
        drag_arrow[0] = drag_arrow_1;
        drag_arrow[1] = drag_arrow_2;
        dragLine = new LineGraphSeries<DataPoint>(drag_arrow);
        dragLine.setThickness(10);
        dragLine.setColor(Color.DKGRAY);
        dragLine.setTitle("Drag");
        airfoil_graph.addSeries(dragLine);


        LineGraphSeries<DataPoint> wind1 = new LineGraphSeries<DataPoint>(new DataPoint[] {
                new DataPoint(0, 0.23),
                new DataPoint(0.15, 0.23),
                new DataPoint(0.14, 0.25),
                new DataPoint(0.15, 0.23),
                new DataPoint(0.14, 0.21),
        });
        wind1.setColor(Color.BLACK);
        airfoil_graph.addSeries(wind1);
        LineGraphSeries<DataPoint> wind2 = new LineGraphSeries<DataPoint>(new DataPoint[] {
                new DataPoint(0, 0.31),
                new DataPoint(0.15, 0.31),
                new DataPoint(0.14, 0.33),
                new DataPoint(0.15, 0.31),
                new DataPoint(0.14, 0.29),
        });
        wind2.setColor(Color.BLACK);
        airfoil_graph.addSeries(wind2);
        LineGraphSeries<DataPoint> wind3 = new LineGraphSeries<DataPoint>(new DataPoint[] {
                new DataPoint(0, 0.15),
                new DataPoint(0.15, 0.15),
                new DataPoint(0.14, 0.17),
                new DataPoint(0.15, 0.15),
                new DataPoint(0.14, 0.13),
        });
        wind3.setColor(Color.BLACK);
        airfoil_graph.addSeries(wind3);
        LineGraphSeries<DataPoint> wind4 = new LineGraphSeries<DataPoint>(new DataPoint[] {
                new DataPoint(0, -0.23),
                new DataPoint(0.15, -0.23),
                new DataPoint(0.14, -0.25),
                new DataPoint(0.15, -0.23),
                new DataPoint(0.14, -0.21),
        });
        wind4.setColor(Color.BLACK);
        airfoil_graph.addSeries(wind4);
        LineGraphSeries<DataPoint> wind5 = new LineGraphSeries<DataPoint>(new DataPoint[] {
                new DataPoint(0, -0.31),
                new DataPoint(0.15, -0.31),
                new DataPoint(0.14, -0.33),
                new DataPoint(0.15, -0.31),
                new DataPoint(0.14, -0.29),
        });
        wind5.setColor(Color.BLACK);
        airfoil_graph.addSeries(wind5);
        LineGraphSeries<DataPoint> wind6 = new LineGraphSeries<DataPoint>(new DataPoint[] {
                new DataPoint(0, -0.15),
                new DataPoint(0.15, -0.15),
                new DataPoint(0.14, -0.17),
                new DataPoint(0.15, -0.15),
                new DataPoint(0.14, -0.13),
        });
        wind6.setColor(Color.BLACK);
        airfoil_graph.addSeries(wind6);

        seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            int progress = 0;
            double alpha = 0;
            double cl = 0;
            double cd = 0;
            int size_cl = 12;
            int size_cd = 12;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;

                // NACA 0012 airfoil
                alpha = (double)progress / seekBar.getMax() * 40. - 20.;
                set_alpha = alpha;

                if (airfoil_type == 0) {
                    cl = -2E-06 * Math.pow(alpha, 4) - 0.0002 * Math.pow(alpha, 3) + 0.0006 * Math.pow(alpha, 2) + 0.1132 * alpha;
                    cd = 8E-07 * Math.pow(alpha, 4) + 2E-06 * Math.pow(alpha, 3) + 0.0003 * Math.pow(alpha, 2) - 0.001 * alpha + 0.01;
                }
                else {
                    // DU21_A17 airfoil
                    cl = 2E-09 * Math.pow(alpha, 6) + 5E-07 * Math.pow(alpha, 5) + 4E-06 * Math.pow(alpha, 4) - 0.0004 * Math.pow(alpha, 3) - 0.0024 * Math.pow(alpha, 2) + 0.1447 * alpha + 0.4665;
                    cd = -5E-09 * Math.pow(alpha, 6) - 2E-08 * Math.pow(alpha, 5) + 3E-06 * Math.pow(alpha, 4) + 1E-05 * Math.pow(alpha, 3) - 9E-05 * Math.pow(alpha, 2) - 0.0004 * alpha + 0.0075;
                }


                LD = cl/cd;
//                textLD.setText(String.format("%.2f", LD));

                size_cl = (int)(Math.abs(cl)*8) + 14;
                size_cd = (int)(Math.abs(cd)*40) + 14;
                textDrag.setTextSize(size_cd);
                textLift.setTextSize(size_cl);
                textAngle.setText(String.format("%.2f", alpha));
                textLift.setText(String.format("%.4f", cl));
                textDrag.setText(String.format("%.4f", cd));
                airfoilImage.setRotation((float) alpha);
                //
                if (airfoil_type == 0) {
                    topSurface.resetData(generateAirfoil(1, alpha));
                    bottomSurface.resetData(generateAirfoil(-1, alpha));
                }
                else{
                    topSurface.resetData(generateAirfoilDU(1, alpha));
                    bottomSurface.resetData(generateAirfoilDU(-1, alpha));
                }

                DataPoint[] lift_arrow = new DataPoint[2];
                double angle = Math.toRadians(Math.abs(alpha));
                double x_lift_new = 0.25*Math.cos(angle) + 0.0*Math.sin(angle);
                double y_lift_new = -0.25*Math.sin(angle) + 0.0*Math.cos(angle);
                double x_lift_new_2 = 0.25*Math.cos(angle) + cd*Math.sin(angle)+0.02;
                double y_lift_new_2 = -0.25*Math.sin(angle) + cl*0.25*Math.cos(angle);

                if (alpha > 0.0) {
                    DataPoint lift_arrow_1 = new DataPoint(x_lift_new, y_lift_new);
                    DataPoint lift_arrow_2 = new DataPoint(x_lift_new, y_lift_new_2);
                    lift_arrow[0] = lift_arrow_1;
                    lift_arrow[1] = lift_arrow_2;
                    liftLine.resetData(lift_arrow);
//
                    DataPoint[] drag_arrow = new DataPoint[2];
                    DataPoint drag_arrow_1 = new DataPoint(x_lift_new, y_lift_new);
                    DataPoint drag_arrow_2 = new DataPoint(x_lift_new_2, y_lift_new);
                    drag_arrow[0] = drag_arrow_1;
                    drag_arrow[1] = drag_arrow_2;
                    dragLine.resetData(drag_arrow);
                }
                else{
                    DataPoint lift_arrow_1 = new DataPoint(x_lift_new, -y_lift_new);
                    DataPoint lift_arrow_2 = new DataPoint(x_lift_new, y_lift_new_2);
                    lift_arrow[0] = lift_arrow_1;
                    lift_arrow[1] = lift_arrow_2;
                    liftLine.resetData(lift_arrow);
//
                    DataPoint[] drag_arrow = new DataPoint[2];
                    DataPoint drag_arrow_1 = new DataPoint(x_lift_new, -y_lift_new);
                    DataPoint drag_arrow_2 = new DataPoint(x_lift_new_2, -y_lift_new);
                    drag_arrow[0] = drag_arrow_1;
                    drag_arrow[1] = drag_arrow_2;
                    dragLine.resetData(drag_arrow);
                }
//                airfoil_graph.addSeries(new LineGraphSeries(generateAirfoil(-1, alpha)));

                double topStall;
                double bottomStall;
                if (airfoil_type == 0){
                    topStall = 13.6;
                    bottomStall = -14.0;
                }
                else{
                    topStall = 11.6;
                    bottomStall = -16.0;
                }

                if (alpha > topStall){
                    textStall.setTextColor(Color.RED);
                }
                else if (alpha < bottomStall){
                    textStall.setTextColor(Color.RED);
                }
                else {
                    textStall.setTextColor(0xffff8800);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }


        });


        }

    public void LDButton(View view){
        textLD.setText(String.format("%.2f", LD));
        double topLD;
        if (airfoil_type == 0){
            topLD = 41.8;
        }
        else{
            topLD = 174.0;
        }
        if (LD >= topLD){
            textLDmessage.setText("Success!");
            textLDmessage.setTextColor(Color.GREEN);
        }
        else {
            textLDmessage.setText("Not quite!");
            textLDmessage.setTextColor(Color.RED);
        }
    }

    public DataPoint[] generateAirfoil(int sign, double angle) {
        int count = 100;
        double alpha = Math.toRadians(angle);
        double count2 = (double)count;
        DataPoint[] values = new DataPoint[count+1];
        for (int i=0; i<count; i++) {
            double x = i /count2;
            double y = sign*0.6*(0.2969*Math.sqrt(x) - 0.1260*x - 0.3516*Math.pow(x, 2) + 0.2843*Math.pow(x, 3) - 0.1015*Math.pow(x, 4));
            double x_new = x*Math.cos(alpha) + y*Math.sin(alpha);
            double y_new = -x*Math.sin(alpha) + y*Math.cos(alpha);
            DataPoint v = new DataPoint(x_new, y_new);
            values[i] = v;
        }
        double x = 1.0;
        double y = sign*0.6*(0.2969*Math.sqrt(x) - 0.1260*x - 0.3516*Math.pow(x, 2) + 0.2843*Math.pow(x, 3) - 0.1015*Math.pow(x, 4));
        double x_new = x*Math.cos(alpha) + y*Math.sin(alpha);
        double y_new = -x*Math.sin(alpha) + y*Math.cos(alpha);
        DataPoint v = new DataPoint(x_new,y_new);
        values[count] = v;

        return values;
    }

    public DataPoint[] generateAirfoilDU(int sign, double angle) {
        int count = 100;
        double alpha = Math.toRadians(angle);
        double count2 = (double)count;
        DataPoint[] values = new DataPoint[count+1];
        for (int i=0; i<count; i++) {
            double x = i /count2;
            double y;
            if (sign==1) {
                y = 0.8*(-8.9974 * Math.pow(x, 6) + 28.543 * Math.pow(x, 5) - 34.92 * Math.pow(x, 4) + 21.026 * Math.pow(x, 3) - 6.9848 * Math.pow(x, 2) + 1.332 * x);
            }
            else{
                y = 0.8*(8.7379*Math.pow(x,6) - 27.937*Math.pow(x,5) + 33.895*Math.pow(x,4) - 20.063*Math.pow(x,3) + 6.507*Math.pow(x,2) - 1.1384*x);
            }
            double x_new = x*Math.cos(alpha) + y*Math.sin(alpha);
            double y_new = -x*Math.sin(alpha) + y*Math.cos(alpha);
            DataPoint v = new DataPoint(x_new, y_new);
            values[i] = v;
        }
        double x = 1.0;
        double y = sign*0.6*(0.2969*Math.sqrt(x) - 0.1260*x - 0.3516*Math.pow(x, 2) + 0.2843*Math.pow(x, 3) - 0.1015*Math.pow(x, 4));
        double x_new = x*Math.cos(alpha) + y*Math.sin(alpha);
        double y_new = -x*Math.sin(alpha) + y*Math.cos(alpha);
        DataPoint v = new DataPoint(x_new,y_new);
        values[count] = v;

        return values;
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
