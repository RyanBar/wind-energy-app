package com.mecheng.ryanbarr.windenergy;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.jjoe64.graphview.series.DataPoint;

/**
 * Created by ryanbarr on 9/3/15.
 */



public class ActivityWindPower extends AppCompatActivity {

    public TextView textWindSpeed;
    public TextView textRadius;
    public TextView textEnergy;
    public TextView textMessage;
    public TextView textRate;
    public ImageView imageTurbine;
    public double alpha = 0;
    public int windSpeedNum = 4;
    public double windSpeed = 10;
    public double bladeRadius = 40;
    public int bladeRadiusNum = 5;
    public double rotationRate = 2.67;
    public int rotationRateNum = 5;
    public double AEP = 400;
    public Switch switchOn;
    public int turnOn = 0;
    public SeekBar seekWind;
    public SeekBar seekRadius;
    public SeekBar seekRate;
    public int AEP_num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wind_power);


        imageTurbine = (ImageView) findViewById(R.id.imageTurbine);
        textWindSpeed = (TextView) findViewById(R.id.textWindSpeed);
        textRadius = (TextView) findViewById(R.id.textRadius);
        textEnergy = (TextView) findViewById(R.id.textEnergy);
        textRate = (TextView) findViewById(R.id.textRate);
        textMessage = (TextView) findViewById(R.id.textMessage);
        switchOn = (Switch) findViewById(R.id.switchOn);


        seekWind = (SeekBar) findViewById(R.id.seekWind);
        seekRadius = (SeekBar) findViewById(R.id.seekRadius);
        seekRate = (SeekBar) findViewById(R.id.seekRate);

        final double AEP_from_RotorSE[] = {8083747.639581,	7243872.887745,	6035437.366997,	3688369.842313,	9195546.0063,	7520339.941624,	6048529.269684,	3640476.585846,	9332660.841288,	7500643.102531,	6013922.267808,	3602784.235248,	9319276.166782,	7475594.099467,	5989809.35662,	3581788.457524,	9302121.408397,	7458946.483262,	5968982.455032,	3560475.741503,	9288319.528496,	7442552.184182,	5956765.218358,	3564902.681169,	9276873.558965,	7434097.94492,	5942876.84632,	3572726.769475,	9268602.403532,	7423861.975818,	5944839.419337,	3580392.021547,	9262921.295741,	7420803.334456,	5948562.338273,	3580900.733132,	9256263.571447,	7422824.072953,	5953280.026806,	3580900.733132,	12457365.756365,	10586360.521591,	8506050.364502,	5039271.177517,	13581342.897149,	10759191.489195,	8491610.599809,	4959936.733675,	13667876.644747,	10719053.621752,	8434493.811295,	4896216.16752,	13640028.343331,	10677713.353855,	8393973.95707,	4860289.370521,	13611833.722064,	10649699.025566,	8358684.269543,	4825402.500848,	13588773.413288,	10621963.384293,	8337873.573266,	4834267.020012,	13569456.586753,	10607510.040492,	8315164.206809,	4845362.879931,	13555404.992738,	10590491.045407,	8319167.266787,	4848883.556786,	13545731.68043,	10585902.147007,	8325771.064728,	4848763.625874,	13534680.739009,	10589932.924328,	8330737.712443,	4848763.625874,	17558154.520775,	14293203.259218,	11281763.190817,	6520090.514994,	18510894.821071,	14421102.880244,	11231625.381482,	6394815.322399,	18571807.454503,	14350719.856479,	11141509.767356,	6293079.223647,	18522545.284228,	14285491.297258,	11077007.135628,	6235839.727759,	18478185.385004,	14240868.500181,	11020612.74786,	6188168.825012,	18441569.54025,	14196583.624403,	10987759.280325,	6203303.669851,	18410746.683662,	14173586.506291,	10956772.450033,	6208055.444714,	18388286.48332,	14148096.758065,	10964833.719348,	6199750.505437,	18372999.158976,	14144545.379873,	10971191.091192,	6198888.277813,	18356515.144078,	14150773.297407,	10970115.225611,	6198888.277813,	20503171.532667,	16856022.585423,	13504870.029931,	7930596.08975,	21275115.78505,	16950010.775791,	13402525.254653,	7740435.850451,	21310492.237487,	16836423.662432,	13265863.184752,	7586169.019078,	21230647.365869,	16737404.659895,	13167850.924882,	7502067.858377,	21163398.628494,	16669613.933076,	13082457.641446,	7492317.220581,	21107722.328077,	16602544.01286,	13035120.99633,	7504404.168571,	21060991.127244,	16568911.383967,	13031726.449813,	7493620.609525,	21027038.693393,	16542030.183818,	13041769.97168,	7479685.580862,	21005009.911905,	16562133.993079,	13039326.793216,	7478441.20306,	20987729.345266,	16565141.581797,	13031853.682844,	7478441.20306,	22394551.418927,	18653938.22959,	15199143.094011,	9160453.591085,	23027481.770436,	18701244.061966,	15022400.848507,	8881652.434226,	23024199.557202,	18528162.010372,	14822091.351738,	8657708.468122,	22902219.714331,	18382968.094572,	14678633.554067,	8542832.427354,	22803668.620616,	18283801.355048,	14556036.8006,	8632855.823131,	22722027.151394,	18187290.575022,	14494075.704273,	8624924.521339,	22654034.772138,	18141635.222836,	14564233.241465,	8607227.747282,	22605855.92771,	18174751.89308,	14563169.535705,	8587898.223367,	22576247.683937,	18189375.250338,	14553389.402034,	8586156.941598,	22603783.713707,	18183843.279968,	14543222.180898,	8586156.941598,	23949313.698811,	20191882.961575,	16682412.777525,	10275425.859135,	24471292.363781,	20170712.612269,	16404395.904992,	9879211.582945,	24411440.592442,	19917914.573516,	16119483.56355,	9570090.477062,	24232913.252773,	19711504.463757,	15917070.775063,	9470278.413442,	24092756.688045,	19571783.925583,	15750822.433978,	9467663.89597,	23976982.111724,	19440724.540197,	15737418.512982,	9445476.010912,	23882991.411764,	19394646.050834,	15714882.65553,	9421789.404601,	23818214.96066,	19430061.830157,	15702368.585346,	9395057.902858,	23802530.765293,	19397976.592646,	15689483.64534,	9392621.052458,	23823795.369594,	19388959.698657,	15675611.56225,	9392621.052458,	25282625.475916,	21545163.284685,	17993947.638649,	11264600.926654,	25704421.440241,	21423038.853545,	17581516.811762,	10718462.291782,	25563684.504379,	21065462.326398,	17187319.806216,	10307474.654539,	25310797.384013,	20780436.390315,	16914572.249594,	10307489.772441,	25116869.128751,	20592973.358999,	16695022.273662,	10093894.180958,	24958999.571228,	20419306.415855,	16728524.840925,	10064929.037782,	24834262.239205,	20449310.077871,	16590530.230877,	10033384.84517,	24749652.667884,	20361531.650503,	16573690.396135,	9997080.12744,	24786320.60829,	20316562.464146,	16556870.140446,	9993745.283412,	24723199.3708,	20304907.650725,	16538245.102252,	9993745.283412,	26446176.817341,	22745904.066337,	19150681.031146,	12113207.853855,	26769900.328969,	22485388.955772,	18563381.955328,	11390825.020283,	26520644.944482,	21993855.423694,	18036833.813894,	10855243.580771,	26172690.635886,	21616270.135089,	17681695.792315,	10794171.537366,	25913394.473631,	21372063.98817,	17417438.117111,	10504730.102322,	25708043.507835,	21149890.761065,	17389910.357283,	10467695.386689,	25544480.632133,	21162810.418736,	17207702.648758,	10426197.519604,	25473564.609071,	21027596.545011,	17186187.255477,	10377747.590291,	25455569.741403,	20969748.002464,	17164258.350852,	10373283.595704,	25367975.548243,	20954887.285784,	17139648.79274,	10373283.595704,	27461751.200412,	23807145.162164,	20163566.95921,	12815003.196881,	27691083.760047,	23376718.82452,	19359602.157424,	11889494.12109,	27309114.698047,	22724434.139484,	18683946.182998,	11220986.627355,	26846309.596261,	22241543.327762,	18226622.453785,	11052366.666455,	26514270.54516,	21926740.95731,	18011518.973362,	10687061.215608,	26250495.078921,	21735602.10536,	17803102.092617,	10640176.070759,	26041911.080265,	21617808.111529,	17572809.889565,	10586358.360016,	25994459.530034,	21447201.202207,	17545751.403893,	10522847.699056,	25895260.98255,	21373612.42252,	17517443.205138,	10516991.483425,	25785166.947292,	21354673.145146,	17485365.694925,	10516991.483425,	28352220.716169,	24744019.031751,	21051897.990297,	13398173.618712,	28489902.047846,	24133735.663408,	20014034.764331,	12224862.137417,	27969452.526513,	23303647.297979,	19158807.352672,	11716633.867311,	27380155.399327,	22691474.495343,	18580982.420364,	11257685.264492,	26960389.656639,	22294594.287413,	18397481.181525,	10803132.286248,	26625565.360758,	22163076.085627,	18123123.288215,	10743956.226193,	26376801.695734,	21970368.979314,	17835983.304733,	10674869.397976,	26358991.90743,	21757835.221552,	17802115.816018,	10592592.065592,	26231559.730157,	21666547.863567,	17765957.101677,	10584992.193219,	26094060.33544,	21642463.980331,	17724563.717389,	10584992.193219, 0.0, 0.0, 0.0};

//        Button button_liftdrag = (Button)findViewById(R.id.button_liftdrag);
//        button_liftdrag.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent_a = new Intent(getApplicationContext(), ActivityLiftDrag.class);
//                startActivity(intent_a);
//            }
//        });
//
//        Button button_windfarm = (Button)findViewById(R.id.button_windfarm);
//        button_windfarm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent_a = new Intent(getApplicationContext(), ActivityWindFarm.class);
//                startActivity(intent_a);
//            }
//        });

        Button button_main = (Button)findViewById(R.id.buttonMenu);
        button_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_a = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent_a);
            }
        });


        switchOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    turnOn = 1;


                    new CountDownTimer(4000, 40) {

                        public void onTick(long millisUntilFinished) {

                            imageTurbine.setRotation((float) alpha);
                            alpha = alpha + rotationRate*5.0;
                            seekWind.setEnabled(false);
                            seekRadius.setEnabled(false);
                            seekRate.setEnabled(false);
                        }

                        public void onFinish() {
                            seekWind.setEnabled(true);
                            seekRadius.setEnabled(true);
                            seekRate.setEnabled(true);
                            switchOn.setChecked(false);
                            double tip_speed = bladeRadius*rotationRate/windSpeed;
                            if (windSpeed > 10.0 || rotationRate > 3.6){
                                AEP_num = 400;
                            }
                            else if (windSpeed < 5.0){
                                AEP_num = 401;
                            }
                            else if (bladeRadius >  80.0){
                                AEP_num = 402;
                            }
                            if (AEP_num == 401 || tip_speed <= 2.0) {
                                textMessage.setText("Your wind turbine wasn't fast enough to make energy!");
                                AEP = 0.0;
                                textEnergy.setText(String.format("%.2f", AEP));
                                textMessage.setTextColor(Color.RED);
                            }
                            else if (AEP_num == 400 || tip_speed >= 25.0){
                                textMessage.setText("Your wind turbine was ripped to shreds by the wind!");
                                AEP = 0.0;
                                textEnergy.setText(String.format("%.2f", AEP));
                                textMessage.setTextColor(Color.RED);

                                TranslateAnimation animation = new TranslateAnimation(0.0f, 4800.0f,
                                        0.0f, -3600.0f);          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
                                animation.setDuration(2500);  // animation duration
//                    animation.setRepeatCount(1);  // animation repeat count
                                animation.setRepeatMode(1);   // repeat animation (left to right, right to left )
                                imageTurbine.startAnimation(animation);
                            }
                            else if (AEP_num == 402){
                                textMessage.setText("Your wind turbine blade broken in half!");
                                AEP = 0.0;
                                textEnergy.setText(String.format("%.2f", AEP));
                                textMessage.setTextColor(Color.RED);
                                TranslateAnimation animation = new TranslateAnimation(0.0f, -4800.0f,
                                        0.0f, 1200.0f);          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
                                animation.setDuration(2500);  // animation duration
//                    animation.setRepeatCount(1);  // animation repeat count
                                animation.setRepeatMode(1);   // repeat animation (left to right, right to left )
                                imageTurbine.startAnimation(animation);
                            }
                            else{
                                AEP = AEP_from_RotorSE[AEP_num];
                                if (AEP>27500000){
                                    int homes = (int)(AEP/10908);
                                    textMessage.setText("The wind turbine is optimized! You can power " +  homes + " homes!");
                                    textEnergy.setText(String.format("%.2f", AEP/1.0e3));
                                    textMessage.setTextColor(Color.GREEN);
                                }
                                else {
                                    int homes = (int) (AEP / 10908);
                                    textMessage.setText("Your wind turbine can power " + homes + " homes!");
                                    textEnergy.setText(String.format("%.2f", AEP / 1.0e3));
                                    textMessage.setTextColor(Color.GREEN);
                                }
                            }

//                            }
                        }
                    }.start();
                    imageTurbine.setRotation((float) 30.0);

                } else {
                    turnOn = 0;
                }
            }
        });




        seekWind.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                windSpeedNum = (4 - progress);
                AEP_num = 40*bladeRadiusNum + 4*rotationRateNum + windSpeedNum;

                if (progress == 0){
                    windSpeed = 2.5;
                    AEP_num = 401;
                }
                else if (progress == 1){
                    windSpeed = 6.0;
                }
                else if (progress == 2){
                    windSpeed = 7.5;
                }
                else if (progress == 3){
                    windSpeed = 8.5;
                }
                else if (progress == 4){
                    windSpeed = 10;
                }
                else {
                    windSpeed = 15;
                    AEP_num = 400;
                }
                textWindSpeed.setText(String.format("%.2f", windSpeed));



            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }


        });



        seekRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                bladeRadiusNum = progress;
                bladeRadius = progress*45.0/seekBar.getMax() + 40.0;
                textRadius.setText(String.format("%.2f", bladeRadius));
                AEP_num = 40*bladeRadiusNum + 4*rotationRateNum + windSpeedNum;

                if (bladeRadius > 80.0){
                    AEP_num = 402;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }


        });

        seekRate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                rotationRateNum = progress;
                rotationRate = progress*3.0/seekBar.getMax() + 1.0;
                textRate.setText(String.format("%.2f", rotationRate));


                AEP_num = 40*bladeRadiusNum + 4*rotationRateNum + windSpeedNum;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }


        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
