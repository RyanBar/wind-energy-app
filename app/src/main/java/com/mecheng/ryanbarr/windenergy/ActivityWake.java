package com.mecheng.ryanbarr.windenergy;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.GridLabelRenderer;

import java.io.File;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Created by jthomas on 12/2/15.
 */
public class ActivityWake extends AppCompatActivity {

    public SeekBar seekVariable;
    public int progressVal;
    public Switch switchVariable;
    public boolean activityIsYaw;
    public double yawAngle;
    public double yPosition;
    public TextView textSeekBarVariable;
    public TextView textSeekBarValue;
    public TextView textSeekBarUnits;
    public TextView textPowA;
    public TextView textPowB;
    public TextView textPowT;
    public TextView textCheck_message;
    public ImageView imageTurbineA;
    public float wakeCent=(float) -10.5, wakeRad=(float) (241.4/2.), powA=(float) 1725.49, powB=(float) 812.67, powT=(float) 2538.16;
    public GraphView wake_graph;
    public LineGraphSeries<DataPoint> topLine;
    public LineGraphSeries<DataPoint> rotLine;
    public LineGraphSeries<DataPoint> botLine;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wake);

        Button button_main = (Button)findViewById(R.id.buttonMenu);
        button_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_a = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent_a);
            }
        });

        seekVariable = (SeekBar) findViewById(R.id.seekBarVariable);
        switchVariable = (Switch) findViewById(R.id.switchVariable);
        textSeekBarVariable = (TextView) findViewById(R.id.textSeekBarVariable);
        textSeekBarValue = (TextView) findViewById(R.id.textSeekBarValue);
        textSeekBarUnits = (TextView) findViewById(R.id.textSeekBarUnits);
        textPowA= (TextView) findViewById(R.id.textPowA);
        textPowB= (TextView) findViewById(R.id.textPowB);
        textPowT= (TextView) findViewById(R.id.textPowT);
        textCheck_message = (TextView) findViewById(R.id.textCheck_message);
        imageTurbineA = (ImageView) findViewById(R.id.imageTurbineA);
        wake_graph = (GraphView) findViewById(R.id.graph);

        switchVariable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                activityIsYaw = isChecked;

                textCheck_message.setTextColor(0xff669900);
                textPowT.setText("___");

                seekVariable.setProgress(50);;
                progressVal = seekVariable.getProgress();

                yPosition = 0.0;
                yawAngle = progressVal*8./10. - 40.;
                yPosition = progressVal*3. - 150.;

                imageTurbineA.setTranslationY((float) -yPosition);

                DataPoint[] top_line = new DataPoint[2];
                if (126.4/2. + yPosition > 150.){
                    top_line[0] = new DataPoint(0.0, 150.);
                }
                else {
                    top_line[0] = new DataPoint(0.0, 126.4/2.+yPosition);
                }
                if (wakeCent+wakeRad+yPosition > 150.){
                    top_line[1] = new DataPoint(8.*126.4, 150.);
                }
                else{
                    top_line[1] = new DataPoint(8.*126.4, wakeCent+wakeRad+yPosition);
                }

                DataPoint[] bot_line = new DataPoint[2];
                bot_line[0] = new DataPoint(0.0, -126.4/2.+yPosition);
                bot_line[1] = new DataPoint(8.*126.4, wakeCent-wakeRad+yPosition);

                topLine.resetData(top_line);
                botLine.resetData(bot_line);

                wakeCent = (float) (0.0006*Math.pow((float) yawAngle, 3.) - (9E-5)*Math.pow((float) yawAngle, 2.) - 2.1613*yawAngle - 10.473);
                wakeRad = (float) ((-0.0186*Math.pow((float) yawAngle, 2) - 7E-15*yawAngle + 241.32)/2.);
                powA = (float) ((4E-05)*Math.pow((float) yawAngle, 4) + (2E-14)*Math.pow((float) yawAngle, 3) - 0.4923*Math.pow((float) yawAngle, 2) + (7E-11)*yawAngle  + 1725.4);
                powB = (float) ((1E-07)*Math.pow((float) yawAngle,6) + (1E-06)*Math.pow((float) yawAngle,5) - 0.0006*Math.pow((float) yawAngle,4) - 0.0043*Math.pow((float) yawAngle,3) + 1.0152*Math.pow((float) yawAngle,2) + 5.0822*yawAngle + 814.96);
                powT = powA + powB;

                textSeekBarValue.setText(String.format("%.1f", yawAngle));
                textPowA.setText(String.format("%.1f", powA));
                textPowB.setText(String.format("%.1f", powB));

                imageTurbineA.setRotation((float) -yawAngle);
                imageTurbineA.setTranslationX((float) ((1. * wake_graph.getWidth() / 10.) - 21.));

                double angle = yawAngle*Math.PI/180.0;
                double x_top_new = 0.0*Math.cos(angle) - (126.4/2.)*Math.sin(angle);
                double y_top_new = 0.0*Math.sin(angle) + (126.4/2.)*Math.cos(angle);
                double x_bot_new = 0.0*Math.cos(angle) - (-126.4/2.)*Math.sin(angle);
                double y_bot_new = 0.0*Math.sin(angle) + (-126.4/2.)*Math.cos(angle);

                top_line[0] = new DataPoint(x_top_new, y_top_new);
                top_line[1] = new DataPoint(8.*126.4, wakeCent+wakeRad);
                bot_line[0] = new DataPoint(x_bot_new, y_bot_new);
                bot_line[1] = new DataPoint(8.*126.4, wakeCent-wakeRad);

                if (yawAngle >= 0.0){
                    DataPoint[] rotor_line = new DataPoint[2];
                    rotor_line[0] = new DataPoint(x_top_new, y_top_new);
                    rotor_line[1] = new DataPoint(x_bot_new, y_bot_new);
                    rotLine.setBackgroundColor(0xff669900);
                    rotLine.resetData(rotor_line);
                }
                else if (yawAngle < 0.0){
                    DataPoint[] rotor_line = new DataPoint[2];
                    rotor_line[0] = new DataPoint(x_bot_new, y_bot_new);
                    rotor_line[1] = new DataPoint(x_top_new, y_top_new);
                    rotLine.setBackgroundColor(0xff0098cc);
                    botLine.setBackgroundColor(0xff669900);
                    rotLine.resetData(rotor_line);
                }

                topLine.resetData(top_line);
                botLine.resetData(bot_line);

                if (isChecked) {
                    textSeekBarVariable.setText("Yaw Angle");
                    textSeekBarUnits.setText("degrees");
                } else {
                    textSeekBarVariable.setText("Front Turbine Y Position");
                    textSeekBarUnits.setText("meters");
                }

            }
        });

        double wind_scale_y=150.0;
        double wind_scale_x=100.0;
        double wind_trans_x=-75.0;

        Viewport view_wake = wake_graph.getViewport();
        view_wake.setXAxisBoundsManual(true);
        view_wake.setYAxisBoundsManual(true);
        view_wake.setMaxX(900.);
        view_wake.setMinX(-100.);
        view_wake.setMaxY(150.);
        view_wake.setMinY(-150.);

        GridLabelRenderer grid = wake_graph.getGridLabelRenderer();
        grid.setHorizontalLabelsVisible(false);
        grid.setVerticalLabelsVisible(false);
        grid.setGridStyle(GridLabelRenderer.GridStyle.NONE);

        DataPoint[] top_line = new DataPoint[2];
        top_line[0] = new DataPoint(0., 126.4/2.);
        top_line[1] = new DataPoint(8.*126.4, wakeCent+wakeRad);
        DataPoint[] rotor_line = new DataPoint[2];
        rotor_line[0] = new DataPoint(0., 126.4/2.);
        rotor_line[1] = new DataPoint(0, -126.4/2.);
        DataPoint[] bot_line = new DataPoint[2];
        bot_line[0] = new DataPoint(0, -126.4/2.);
        bot_line[1] = new DataPoint(8.*126.4, wakeCent-wakeRad);

        topLine = new LineGraphSeries<DataPoint>(top_line);
        topLine.setThickness(5);
        topLine.setColor(0xff669900);
        rotLine = new LineGraphSeries<DataPoint>(rotor_line);
        rotLine.setThickness(1);
        rotLine.setColor(0xff669900);
        botLine = new LineGraphSeries<DataPoint>(bot_line);
        botLine.setThickness(5);
        botLine.setColor(0xff669900);


        topLine.setDrawBackground(true);
        botLine.setDrawBackground(true);
        rotLine.setDrawBackground(true);

        topLine.setBackgroundColor(0xff0098cc);
        botLine.setBackgroundColor(0xff669900);
        rotLine.setBackgroundColor(0xff669900);

        wake_graph.addSeries(topLine);
        wake_graph.addSeries(rotLine);
        wake_graph.addSeries(botLine);


        imageTurbineA.setRotation((float) -0.0);

        float aaa = (float)((1. * wake_graph.getWidth() / 10.) - 21.);
//        imageTurbineA.setTranslationX((float) ((1. * wake_graph.getWidth() / 10.) - 21.));
        imageTurbineA.setTranslationX((float) ((87.0)));

//        LineGraphSeries<DataPoint> wind1 = new LineGraphSeries<DataPoint>(new DataPoint[] {
//                new DataPoint(0, 0.23*),
//                new DataPoint(0.15, 0.23),
//                new DataPoint(0.14, 0.25),
//                new DataPoint(0.15, 0.23),
//                new DataPoint(0.14, 0.21),
//        });
//        wind1.setColor(Color.BLACK);
//        wake_graph.addSeries(wind1);
//        LineGraphSeries<DataPoint> wind2 = new LineGraphSeries<DataPoint>(new DataPoint[] {
//                new DataPoint(0, 0.31),
//                new DataPoint(0.15, 0.31),
//                new DataPoint(0.14, 0.33),
//                new DataPoint(0.15, 0.31),
//                new DataPoint(0.14, 0.29),
//        });
//        wind2.setColor(Color.BLACK);
//        wake_graph.addSeries(wind2);
//        LineGraphSeries<DataPoint> wind3 = new LineGraphSeries<DataPoint>(new DataPoint[] {
//                new DataPoint(0, 0.15),
//                new DataPoint(0.15, 0.15),
//                new DataPoint(0.14, 0.17),
//                new DataPoint(0.15, 0.15),
//                new DataPoint(0.14, 0.13),
//        });
//        wind3.setColor(Color.BLACK);
//        wake_graph.addSeries(wind3);
//        LineGraphSeries<DataPoint> wind4 = new LineGraphSeries<DataPoint>(new DataPoint[] {
//                new DataPoint(0, -0.23),
//                new DataPoint(0.15, -0.23),
//                new DataPoint(0.14, -0.25),
//                new DataPoint(0.15, -0.23),
//                new DataPoint(0.14, -0.21),
//        });
//        wind4.setColor(Color.BLACK);
//        wake_graph.addSeries(wind4);
//        LineGraphSeries<DataPoint> wind5 = new LineGraphSeries<DataPoint>(new DataPoint[] {
//                new DataPoint(0, -0.31),
//                new DataPoint(0.15, -0.31),
//                new DataPoint(0.14, -0.33),
//                new DataPoint(0.15, -0.31),
//                new DataPoint(0.14, -0.29),
//        });
//        wind5.setColor(Color.BLACK);
//        wake_graph.addSeries(wind5);
//        LineGraphSeries<DataPoint> wind6 = new LineGraphSeries<DataPoint>(new DataPoint[] {
//                new DataPoint(0, -0.15),
//                new DataPoint(0.15, -0.15),
//                new DataPoint(0.14, -0.17),
//                new DataPoint(0.15, -0.15),
//                new DataPoint(0.14, -0.13),
//        });
//        wind6.setColor(Color.BLACK);
//        wake_graph.addSeries(wind6);

        seekVariable.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 50;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                progressVal = progress;

                if (activityIsYaw) {
                    yPosition = 0.0;
                    yawAngle = progressVal*8./10. - 40.;

                    wakeCent = (float) (0.0006*Math.pow((float) yawAngle, 3.) - (9E-5)*Math.pow((float) yawAngle, 2.) - 2.1613*yawAngle - 10.473);
                    wakeRad = (float) ((-0.0186*Math.pow((float) yawAngle, 2) - 7E-15*yawAngle + 241.32)/2.);
                    powA = (float) ((4E-05)*Math.pow((float) yawAngle, 4) + (2E-14)*Math.pow((float) yawAngle, 3) - 0.4923*Math.pow((float) yawAngle, 2) + (7E-11)*yawAngle  + 1725.4);
                    powB = (float) ((1E-07)*Math.pow((float) yawAngle,6) + (1E-06)*Math.pow((float) yawAngle,5) - 0.0006*Math.pow((float) yawAngle,4) - 0.0043*Math.pow((float) yawAngle,3) + 1.0152*Math.pow((float) yawAngle,2) + 5.0822*yawAngle + 814.96);
                    powT = powA + powB;

                    textSeekBarValue.setText(String.format("%.1f", yawAngle));
                    textPowA.setText(String.format("%.1f", powA));
                    textPowB.setText(String.format("%.1f", powB));

                    imageTurbineA.setRotation((float) -yawAngle);

                    float bbb = (float)((1. * wake_graph.getWidth() / 10.) - 21.);
                    imageTurbineA.setTranslationX((float) ((1. * wake_graph.getWidth() / 10.) - 21.));

                    double angle = yawAngle*Math.PI/180.0;
                    double x_top_new = 0.0*Math.cos(angle) - (126.4/2.)*Math.sin(angle);
                    double y_top_new = 0.0*Math.sin(angle) + (126.4/2.)*Math.cos(angle);
                    double x_bot_new = 0.0*Math.cos(angle) - (-126.4/2.)*Math.sin(angle);
                    double y_bot_new = 0.0*Math.sin(angle) + (-126.4/2.)*Math.cos(angle);

                    DataPoint[] top_line = new DataPoint[2];
                    top_line[0] = new DataPoint(x_top_new, y_top_new);
                    top_line[1] = new DataPoint(8.*126.4, wakeCent+wakeRad);
                    DataPoint[] bot_line = new DataPoint[2];
                    bot_line[0] = new DataPoint(x_bot_new, y_bot_new);
                    bot_line[1] = new DataPoint(8.*126.4, wakeCent-wakeRad);
                    if (yawAngle >= 0.0){
                        DataPoint[] rotor_line = new DataPoint[2];
                        rotor_line[0] = new DataPoint(x_top_new, y_top_new);
                        rotor_line[1] = new DataPoint(x_bot_new, y_bot_new);
                        rotLine.setBackgroundColor(0xff669900);
                        rotLine.resetData(rotor_line);
                    }
                    else if (yawAngle < 0.0){
                        DataPoint[] rotor_line = new DataPoint[2];
                        rotor_line[0] = new DataPoint(x_bot_new, y_bot_new);
                        rotor_line[1] = new DataPoint(x_top_new, y_top_new);
                        rotLine.setBackgroundColor(0xff0098cc);
                        botLine.setBackgroundColor(0xff669900);
                        rotLine.resetData(rotor_line);
                    }


                    topLine.resetData(top_line);
                    botLine.resetData(bot_line);
                }

                else {
                    yPosition = progressVal*3. - 150.;

                    wakeCent = (float) (yPosition - 10.449);
                    wakeRad = (float) (241.42/2.);
                    powA = (float) (1725.6);
//                    powB = (float) ((6E-11)*Math.pow((float) yPosition, 6) - (4E-09)*Math.pow((float) yPosition, 5) - (4E-06)*Math.pow((float) yPosition, 4) + 0.0002*Math.pow((float) yPosition, 3) + 0.1074*Math.pow((float) yPosition, 2) - 2.2814*yPosition + 817.22
                    powB = (float) (812.59+(1725.57-812.59)*(1.-Math.cos(3.1415927*(yPosition-10.41)/161.41))/2.);
                    powT = powA + powB;

                    textSeekBarValue.setText(String.format("%.1f", yPosition));
                    textPowA.setText(String.format("%.1f", powA));
                    textPowB.setText(String.format("%.1f", powB));

                    imageTurbineA.setTranslationY((float) -yPosition);

                    imageTurbineA.setTranslationX((float) ((1. * wake_graph.getWidth() / 10.) - 21.));

                    DataPoint[] top_line = new DataPoint[2];
                    if (126.4/2. + yPosition > 150.){
                        top_line[0] = new DataPoint(0.0, 150.);
                    }
                    else {
                        top_line[0] = new DataPoint(0.0, (126.4/2.)+yPosition);
                    }
                    if (wakeCent+wakeRad+yPosition > 150.){
                        top_line[1] = new DataPoint(8.*126.4, 150.);
                    }
                    else{
                        top_line[1] = new DataPoint(8.*126.4, wakeCent+wakeRad);
                    }

                    DataPoint[] bot_line = new DataPoint[2];
                    bot_line[0] = new DataPoint(0.0, (-126.4/2.)+yPosition);
//                    bot_line[1] = new DataPoint(8.*126.4, wakeCent-wakeRad+yPosition);
                    bot_line[1] = new DataPoint(8.*126.4, wakeCent-wakeRad);

                    topLine.resetData(top_line);
                    botLine.resetData(bot_line);
                }



//                if (yawAngle >= 0) {
//                    rotLine.setBackgroundColor(0xffcc2200);
//                }
//                else if (yawAngle < 0){
//
//                    rotLine.setBackgroundColor(0xffaa66cc);
//                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }


        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void CheckButton(View view){

        textPowT.setText(String.format("%.2f", powT));

        double powMax;

        if (activityIsYaw) {
            powMax = 2745.;
        } else {
            powMax = 3450.;
        }


        if (powT >= powMax){
            textCheck_message.setText("Success!");
            textCheck_message.setTextColor(Color.GREEN);
        }
        else {
            textCheck_message.setText("Keep trying . . .");
            textCheck_message.setTextColor(Color.RED);
        }
    }
}
