package com.mecheng.ryanbarr.windenergy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.content.Intent;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    public ImageView cloud1;
    public ImageView cloud2;
    public ImageView cloud3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button button_airfoil = (Button)findViewById(R.id.button_airfoil);
        button_airfoil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_v = new Intent(getApplicationContext(), ActivityLiftDrag_Instructions.class);
                startActivity(intent_v);
            }
        });
        Button button_wake = (Button)findViewById(R.id.button_wake);
        button_wake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_v = new Intent(getApplicationContext(), ActivityWake_Instructions.class);
                startActivity(intent_v);
            }
        });
        Button button_wind_power = (Button)findViewById(R.id.button_wind_power);
        button_wind_power.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_v = new Intent(getApplicationContext(),ActivityWindPower_Instructions.class);
                startActivity(intent_v);
            }
        });
//        Button button_wind_farm = (Button)findViewById(R.id.button_wind_farm);
//        button_wind_farm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent_h = new Intent(getApplicationContext(), ActivityWindFarm.class);
//                startActivity(intent_h);
//            }
//        });
        Button button_aero = (Button)findViewById(R.id.button_aero);
        button_aero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_a = new Intent(getApplicationContext(), ActivityAero.class);
                startActivity(intent_a);
            }
        });

        cloud1 = (ImageView) findViewById(R.id.imageCloud1);
        cloud2 = (ImageView) findViewById(R.id.imageCloud2);
        cloud3 = (ImageView) findViewById(R.id.imageCloud3);
        TranslateAnimation animation = new TranslateAnimation(400.0f, -1500.0f,
                0.0f, 25.0f);          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
        animation.setDuration(20000);  // animation duration
        animation.setRepeatCount(5);  // animation repeat count
        animation.setRepeatMode(2);   // repeat animation (left to right, right to left )

//      animation.setFillAfter(true);

        TranslateAnimation animation2 = new TranslateAnimation(-400.0f, 1200.0f,
                50.0f, -50.0f);          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
        animation2.setDuration(7000);  // animation duration
        animation2.setRepeatCount(14);  // animation repeat count
        animation2.setRepeatMode(2);   // repeat animation (left to right, right to left )
//      animation.setFillAfter(true);

        TranslateAnimation animation3 = new TranslateAnimation(400.0f, -1200.0f,
                -50.0f, 50.0f);          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
        animation3.setDuration(10000);  // animation duration
        animation3.setRepeatCount(10);  // animation repeat count
        animation3.setRepeatMode(2);   // repeat animation (left to right, right to left )
//      animation.setFillAfter(true);

        cloud1.startAnimation(animation);  // start animation
        cloud2.startAnimation(animation2);  // start animation
        cloud3.startAnimation(animation3);  // start animation


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
