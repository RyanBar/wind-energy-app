package com.mecheng.ryanbarr.windenergy;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.GridLabelRenderer;

/**
 * Created by ryanbarr on 9/3/15.
 */
public class ActivityWindPower_Instructions extends AppCompatActivity {

    public LineGraphSeries<DataPoint> topSurface;
    public GraphView power_graph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wind_power_instructions);

        power_graph = (GraphView) findViewById(R.id.graph);

        Button button_airfoil = (Button)findViewById(R.id.button_airfoil);
        button_airfoil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_v = new Intent(getApplicationContext(), ActivityWindPower.class);
                startActivity(intent_v);
            }
        });

        Viewport view_airfoil = power_graph.getViewport();
        view_airfoil.setXAxisBoundsManual(true);
        view_airfoil.setYAxisBoundsManual(true);
        view_airfoil.setMaxX(14.0);
//        view_airfoil.setMinX(1.9);
        view_airfoil.setMaxY(0.6);
        view_airfoil.setScrollable(true);
        view_airfoil.setScalable(true);
//        view_airfoil.setMinY(-0.35);
        GridLabelRenderer grid = power_graph.getGridLabelRenderer();
        grid.setVerticalAxisTitle("power coefficient");
        grid.setHorizontalAxisTitle("tip-speed ratio");
//        grid.setHorizontalLabelsVisible(false);
//        grid.setVerticalLabelsVisible(false);
//        grid.setGridStyle(GridLabelRenderer.GridStyle.NONE);



        int count = 48;
        double count2 = (double)count;
        DataPoint[] values = new DataPoint[count+3];
        values[0] = new DataPoint(0, 0);
        values[1] = new DataPoint(1, 0);
        values[2] = new DataPoint(2, 0);
        for (int i=0; i<count; i++) {
            double x = i/4.0;
            double y;
            y = -8E-05 * Math.pow(x, 5) + 0.0022 * Math.pow(x, 4) - 0.0216 * Math.pow(x, 3) + 0.0763* Math.pow(x, 2) - 0.0029 * x + 0.1551;
            DataPoint v = new DataPoint(x+3, y);
            values[i+3] = v;
        }
        LineGraphSeries<DataPoint> power = new LineGraphSeries<DataPoint>(values);

        LineGraphSeries<DataPoint> betz = new LineGraphSeries<DataPoint>(new DataPoint[] {
                new DataPoint(0, 0.597),
                new DataPoint(14, 0.597),
        });

        LineGraphSeries<DataPoint> cutin = new LineGraphSeries<DataPoint>(new DataPoint[] {
                new DataPoint(2, 0),
                new DataPoint(2, 0.6),
        });
        power_graph.addSeries(power);
        power_graph.addSeries(betz);
        power_graph.addSeries(cutin);
        cutin.setColor(Color.LTGRAY);
        betz.setColor(Color.RED);
        power.setColor(0xffff8800);
        power.setTitle("Power Curve");
        betz.setTitle("Betz's Limit");
        cutin.setTitle("Cut-in Speed");
        power.setThickness(15);
//        power.setBackgroundColor(0xffff8801);
//        power.setDrawBackground(true);


        power_graph.getLegendRenderer().setVisible(true);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
