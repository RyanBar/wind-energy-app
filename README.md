This is code for an Android app called WindVille: Turbine Design. The app is hosted on Google Play at https://play.google.com/store/apps/details?id=com.mecheng.ryanbarr.windenergy&hl=en. The intended audience is for middle school to high school students. The goal is for users to gain intuition for, and learn some theory behind, the fluid dynamic principles of lift, drag, non-dimensional analysis, power extraction from a fluid, and wake effects. There are three activities:

Lift and Drag on an Airfoil - This activity allows the user to change the angle of attack and determine the lift to drag ratio for two airfoils. The objective is to get the highest lift to drag ratio. The lift and drag coefficients are calculated from an airfoil analysis panel method called XFOIL. For more details on XFOIL, see http://web.mit.edu/drela/Public/web/xfoil/. 


Wind Turbine Design - This activity allows the user to effectively change the tip-speed ratio by changing wind speed, rotor radius, and rotation rate. The objective is to get the highest power output. All calculations were performed using RotorSE, part of the WISDEM framework. For more details on RotorSE, see http://wisdem.github.io/RotorSE/. 


Wake Effects - This activity allows the user to change the position or yaw of the front turbine and see the effect of the wake on the back turbine. The objective is to get the highest power output for both turbines. The wake calculations were performed using the FLORIS wake model. For more details on the FLORIS wake model, see https://github.com/WISDEM/FLORISSE. 